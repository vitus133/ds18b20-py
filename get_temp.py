from w1thermsensor import W1ThermSensor

def get_temp():
    return W1ThermSensor().get_temperature()

if __name__ == '__main__':
    print(get_temp())
